# Plantilla para Presentaciones USM
Esta es la plantilla para presentaciones según los formatos y exigencias del **"Manual de Identidad Visual Corporativa"** de la Universidad Técnica Federico Santa María ([Departamento de Industrias](http://www.industrias.usm.cl)).

## Uso
Esta plantilla ocupa *beamer* como base.

Editar el archivo `beamerUSM.tex`, y luego, en una consola (o su editor de latex favorito): 

	$ pdflatex beamerUSM.tex
	$ bibtex bibliography
	$ pdflatex beamerUSM.tex
	$ pdflatex beamerUSM.tex

**NOTA:** `pdflatex` debe ejecutarse (en consola) tres veces, como se indica y en el orden mostrado para que latex pueda construir las Tablas de Contenidos y las referencias cruzadas de la Bibliografía.